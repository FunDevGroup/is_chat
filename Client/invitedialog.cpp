#include "invitedialog.h"
#include "ui_invitedialog.h"
#include "mainwindow.h"
#include <QJsonObject>
#include <QJsonDocument>

inviteDialog::inviteDialog(tcpClient *client, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::inviteDialog)
{
    ui->setupUi(this);
    connect(ui->pbAccept, &QPushButton::clicked, this, &inviteDialog::accept);
    connect(ui->pbDecline, &QPushButton::clicked, this, &inviteDialog::decline);

}

inviteDialog::~inviteDialog()
{
    delete ui;
}

void inviteDialog::inviteMessage(const QString& user, const QString& message, const int &roomId)
{
    QString msg = "Пользователь %1 приглашает вас в группу %2";
    userWhoInvite = user;
    roomWhereInvite = message;
    roomIdInvite = roomId;
    ui->tbInvite->setPlainText(msg.arg(user).arg(message));
}

void inviteDialog::accept()
{
    QJsonObject jobject;
    jobject["commandCode"] = commandCode::inviteaccept;
    jobject["roomId"] = roomIdInvite;
    QJsonDocument doc(jobject);
    client->sendData(doc);
    accept();
}

void inviteDialog::decline()
{
    reject();
}
