#include "tcpclient.h"
#include "mainwindow.h"
#include <QDebug>
#include <QDataStream>
#include <QTcpSocket>
#include <QJsonDocument>
#include <QJsonObject>
#include <QThread>
#include <QMap>


tcpClient::tcpClient()
{
    socket = new QTcpSocket();
    connect(socket, &QTcpSocket::readyRead, this, &tcpClient::newData);
}

void tcpClient::connectToServer(QString &adress)
{
    socket->connectToHost (adress, 5000); //("localhost", 5000); //
    if (socket->waitForConnected(5500)) {
        qDebug() << "Connect Success";
        emit connectSuccess();
    }
    else {
        qDebug() << "connect error" << socket->errorString();
        emit connectError(socket->errorString());
    }
}

void tcpClient::sendData(const QJsonDocument &data)
{
    quint32 packageSize = data.toJson().length();
    QByteArray buffer;
    QDataStream stream(&buffer, QIODevice::ReadWrite);
    QString newSendData = data.toJson();
    quint32 size = newSendData.toUtf8().size();
    stream << size;
    stream << newSendData.toUtf8();
    socket->write(buffer);
    qDebug() << "Packagesize" << packageSize;
    qDebug() << "Size" << size;
    socket->waitForBytesWritten(5000);
    socket->waitForReadyRead(5000);

}

void tcpClient::newData()
{
    //QTcpSocket* socket = static_cast<QTcpSocket*>(sender());
//    while (true) {
//        QThread::sleep(3);
//        if (socket->bytesAvailable() >= 4) {
//            break;
//        }
//    }

    quint32 packageSize;
    QByteArray buffer(socket->read(4));
    QDataStream stream(buffer);
    stream >> packageSize;
    packageSize+=4;


    buffer.clear();
    while (buffer.size() < packageSize) {
        if (socket->bytesAvailable() <= (packageSize - buffer.length())) {
           buffer.append(socket->readAll());
    }
        else {
            buffer.append(socket->read(packageSize - buffer.length()));
        }
    }

    buffer.remove(0, 4);
    emit newDataAvaible(buffer);
}

void tcpClient::doCommand(const QByteArray &data)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);
    int commandCode = doc.object()["commandCode"].toInt();
    emit commandCodeRead(commandCode);
}






















