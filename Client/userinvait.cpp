#include "userinvait.h"
#include "ui_userinvait.h"
#include <QList>

UserInvait::UserInvait(int userId, tcpClient *client, MainWindow *w, QWidget *parent) : QDialog(parent), ui(new Ui::UserInvait)
{
    ui->setupUi(this);
    this->client = client;
    this->w = w;
    connect(ui->pbInvite, &QPushButton::clicked, this, &UserInvait::invite);
    idUser = userId;
    roomList();

}

UserInvait::~UserInvait()
{
    delete ui;
}

void UserInvait::roomList()
{
    QListWidget* lwRoom = w->getLwRooms();
//    QList<QListWidgetItem*> *listRoom;
    auto listRoom = lwRoom->findItems("*", Qt::MatchWildcard);
    foreach (QListWidgetItem *item, listRoom) {
        if (item->data(Qt::UserRole).toInt() == 1) {
            ui->lwRoomsToInvite->addItem(item);
        }
    }
}

void UserInvait::invite()
{
    QListWidgetItem* item =  ui->lwRoomsToInvite->currentItem();
    int roomId = item->data(Qt::UserRole + 1).toInt();
    QJsonObject jobject;
    jobject["commandCode"] = commandCode::invitefromroom;
    jobject["roomId"] = roomId;
    jobject["userId"]= idUser;
    QJsonDocument doc(jobject);
    client->sendData(doc);
    accept();
}
