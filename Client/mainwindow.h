#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMap>
#include "tcpclient.h"
#include "createroomdialog.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

namespace commandCode {
    const int reg = 1;
    const int message = 2;
    const int roomCreated = 3;
    const int invite = 5;
    const int inviteaccept = 6;
    const int invitefromroom = 7;
    const int auth = 10;
    const int invalidAuth = 11;
    const int loginAlreadyUsed = 12;
    const int databaseError = 13;
    const int sendRoomId = 14;
    const int sendUserId = 15;

}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void auth();
    QListWidget* getLwRooms();


private:
    Ui::MainWindow *ui;
    tcpClient* client;

private:
    void dialogWindow();
    void sendMessage();
    void doCommand(const QByteArray& data);
    void clickedRoomItem(QListWidgetItem *item);
    void clickedUserItem();
    void createRoom();
    void roomJsonToMap(const QVariantMap &data);
    void roomList(const QVariantMap &data);
    void userList(const QVariantMap &data);

};
#endif // MAINWINDOW_H
