#ifndef TCPCLIENT_H
#define TCPCLIENT_H
#include <QTcpSocket>
#include <QMap>


class tcpClient: public QObject
{
    Q_OBJECT
public:
    tcpClient();
    void connectToServer(QString &adress);
    void sendData(const QJsonDocument& data);
    void newData();
    void doCommand(const QByteArray& data);
    int sessionId;
    QMap <int, QString> roomIDName;
    QMap <int, QString> userIDName;
    QMap <QString, QString> defaultRoomHistory;

private:
    QTcpSocket * socket;

signals:
    void connectSuccess();
    void connectError(const QString&);
    void newDataAvaible(const QByteArray&);
    void commandCodeRead(int);


};

#endif // TCPCLIENT_H
