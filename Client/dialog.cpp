#include "dialog.h"
#include "ui_dialog.h"
#include "mainwindow.h"

#include <QMessageBox>

Dialog::Dialog(tcpClient *client, QWidget *parent) : QDialog(parent), ui(new Ui::Dialog)
{
    ui->setupUi(this);
    //Dialog::setAttribute(Qt::WA_DeleteOnClose, true);
    this->client = client;    
    connect(ui->pbEnter, &QPushButton::clicked, this, &Dialog::connection);
    connect(ui->pbRegister, &QPushButton::clicked, this, &Dialog::registration);
    connect(client, &tcpClient::newDataAvaible, this, &Dialog::serverAnswer);
}

Dialog::~Dialog()
{
    delete ui;
}

QJsonDocument Dialog::dataToJson(const int data)
{
    QJsonObject jobject;
    jobject["commandCode"] = data;
    jobject["login"] = ui->leLogin->text();
    jobject["password"]= ui->lePass->text();
    jobject["name"]= ui->leName->text();
    QJsonDocument doc(jobject);
    return doc;
}

void Dialog::registration()
{
    QString adress = ui->leSrvAdress->text();
    client->connectToServer(adress);
    client->sendData(dataToJson(commandCode::reg));
    //client->newData();

}

void Dialog::connection()
{
    QString adress = ui->leSrvAdress->text();
    client->connectToServer(adress);
    client->sendData(dataToJson(commandCode::auth));
   // client->newData();

}


void Dialog::serverAnswer(const QByteArray& data)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);


    int commandCode = doc.object()["commandCode"].toInt();
    switch (commandCode) {
        case commandCode::auth:
            client->sessionId = doc.object()["id"].toInt(); //запись ИД пользователя в экземпляр клиента
            authResult = doc.object().toVariantMap();
            emit authSeccess(authResult);
            accept();
            return;
        case commandCode::reg:
            client->sessionId = doc.object()["id"].toInt();
            authResult = doc.object().toVariantMap();
            emit authSeccess(authResult);
            accept();
            return;
        case commandCode::invalidAuth:
            QMessageBox::warning(this, "Ошибка", "Не верный логин или пароль!!!");
            break;
        case commandCode::loginAlreadyUsed:
            QMessageBox::warning(this, "Внимание", "Логин уже используется!\nИспользуйте другой логин");
            break;
        case commandCode::databaseError:
            QMessageBox::warning(this, "Ошибка", "Операция недоступна!\nПопробуйте позднее");
            break;
        }
}































