#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QMessageBox>
#include <QMenu>

#include "dialog.h"
#include "invitedialog.h"
#include "createroomdialog.h"
#include "userinvait.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    client = new tcpClient();
    ui->setupUi(this);
    connect(ui->pbSend, &QPushButton::clicked, this, &MainWindow::sendMessage);
    connect(client, &tcpClient::newDataAvaible, this, &MainWindow::doCommand);
    connect(ui->lwRooms, &QListWidget::itemDoubleClicked, this, &MainWindow::clickedRoomItem);
    connect(ui->pbCreateroom, &QPushButton::clicked, this, &MainWindow::createRoom);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::auth()
{
    Dialog* d = new Dialog(client);
    connect(d, &Dialog::authSeccess, this, &MainWindow::roomJsonToMap);

    d->setWindowModality(Qt::ApplicationModal);
    //int code = d.exec();
    //qDebug() << code;
    d->exec();
    //connect(d, &QDialog::finished, d, &QDialog::deleteLater);

//    qDebug() << d->authResult;
    //    roomJsonToMap(d->authResult);
}

QListWidget *MainWindow::getLwRooms()
{
    return ui->lwRooms;
}

void MainWindow::roomJsonToMap(const QVariantMap &data)
{
    roomList(data["rooms"].toMap());
    userList(data["users"].toMap());
}




void MainWindow::roomList(const QVariantMap &data)
{
    ui->lwRooms->clear();
    QMapIterator<QString, QVariant>imap(data);
    while (imap.hasNext())
    {
        imap.next();
        QVariantMap admin = imap.value().toMap();
        QListWidgetItem* item = new QListWidgetItem(admin["name"].toString(), ui->lwRooms);
        item->setData(Qt::UserRole, admin["isAdmin"].toInt()); //признак админа
        item->setData(Qt::UserRole + 1, imap.key());    //ИД комнаты
        ui->lwRooms->addItem(item);
        ui->lwRooms->sortItems();
    }

}

void MainWindow::userList(const QVariantMap &data)
{
    ui->lwUsers->clear();
    QMapIterator<QString, QVariant>imap(data);
    while (imap.hasNext())
    {
        imap.next();
        QListWidgetItem* item = new QListWidgetItem(imap.value().toString(), ui->lwUsers);
        item->setData(Qt::UserRole, imap.key());
        ui->lwUsers->addItem(item);
        ui->lwUsers->sortItems();
    }
    QAction* action = new QAction("Пригласить в комнату ");
    connect(action, &QAction::triggered, this, &MainWindow::clickedUserItem);
    ui->lwUsers->insertAction(nullptr, action);
    ui->lwUsers->setContextMenuPolicy(Qt::ActionsContextMenu);
}



void MainWindow::sendMessage()
{
    QJsonObject jobject;
    jobject["commandCode"] = commandCode::message;
    jobject["message"] = ui->leData->text();
    QJsonDocument doc(jobject);
    client->sendData(doc);
    qDebug() << "Отправка сообщения "<<doc;
    ui->leData->clear();
}

void MainWindow::doCommand(const QByteArray &data)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);
    //qDebug() << doc;
    int commandCode = doc.object()["commandCode"].toInt();

    switch (commandCode) {
        case commandCode::message: {
            QString user = doc.object()["user"].toString();
            qDebug() << "Eto user" << user;
            QString message = doc.object()["message"].toString();
            ui->leMessage->appendPlainText(QString("%1: %2\n").arg(user).arg(message));//setPlainText(QString("%1: %2\n").arg(user).arg(message));
            break;
        }
//        case commandCode::roomCreated: {
//            int userId = doc.object()["userId"].toInt();
//            QString userName = doc.object()["userName"].toString();
//            QListWidgetItem* item = new QListWidgetItem(userName, ui->lwUsers);
//            item->setData(Qt::UserRole, userId);
//            ui->lwUsers->addItem(item);
//            ui->lwUsers->sortItems();

//            // int userId = item->data(Qt::UserRole).toInt(); получение обратно ID пользователя
//            break;
//        }
//        case commandCode::roomName: {
//            int roomId = doc.object()["roomId"].toInt();
//            QString roomName = doc.object()["roomName"].toString();
//            QListWidgetItem* item = new QListWidgetItem(roomName, ui->lwRooms);
//            item->setData(Qt::UserRole, roomId);
//            ui->lwRooms->addItem(item);
//            ui->lwRooms->sortItems();
//            // int userId = item->data(Qt::UserRole).toInt();
//            break;
//        }
       case commandCode::invite: {
            QString user = doc.object()["user"].toString();
            QString message = doc.object()["room"].toString();
            int roomId = doc.object()["roomId"].toInt();
            inviteDialog* i = new inviteDialog(client);
            i->inviteMessage(user, message, roomId);
            i->exec();
            break;
        }
        case commandCode::sendRoomId: {
            QVariantMap roomClicked = doc.object().toVariantMap();
            userList(roomClicked["users"].toMap());
        }

    }
}

void MainWindow::clickedRoomItem(QListWidgetItem *item)
{
    ui->leMessage->clear();
    int roomId = item->data(Qt::UserRole + 1).toInt();
    QJsonObject jobject;
    jobject["commandCode"] = commandCode::sendRoomId;
    jobject["roomId"] = roomId;
    QJsonDocument doc(jobject);
    client->sendData(doc);
}

void MainWindow::clickedUserItem()
{
    QListWidgetItem *item = ui->lwUsers->currentItem();
    int userId = item->data(Qt::UserRole).toInt();
    UserInvait *u = new UserInvait(userId, client, this);
    u->exec();
}

void MainWindow::createRoom()
{
    createroomdialog *i = new createroomdialog(client);
    connect(i, &createroomdialog::createRoomSuccess, this, &MainWindow::roomJsonToMap);
    i->exec();
}
















