#ifndef CREATEROOMDIALOG_H
#define CREATEROOMDIALOG_H

#include <QDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include "mainwindow.h"

namespace Ui {
class createroomdialog;
}

class createroomdialog : public QDialog
{
    Q_OBJECT

public:
    explicit createroomdialog(tcpClient*client, QWidget *parent = nullptr);
    ~createroomdialog();

private:
    Ui::createroomdialog *ui;
    tcpClient *client;
    void click();
    void serverAnswer(const QByteArray& data);
    QVariantMap createRoomResult;

signals:
    void createRoomSuccess(const QVariantMap&);
};

#endif // CREATEROOMDIALOG_H
