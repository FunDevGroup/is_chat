#ifndef INVITEDIALOG_H
#define INVITEDIALOG_H

#include <QDialog>
#include <QTextBrowser>
#include <QPushButton>
#include "tcpclient.h"

namespace Ui {
class inviteDialog;
}

class inviteDialog : public QDialog
{
    Q_OBJECT

public:
    explicit inviteDialog(tcpClient* client,QWidget *parent = nullptr);
    ~inviteDialog();
    void inviteMessage(const QString &user, const QString &message, const int &roomId);
    void accept();
    void decline();
    QString userWhoInvite, roomWhereInvite;
    int roomIdInvite;

private:
    Ui::inviteDialog *ui;
    tcpClient *client;
};

#endif // INVITEDIALOG_H
