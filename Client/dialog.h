#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "tcpclient.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QMap>
#include <QList>


namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(tcpClient*client, QWidget *parent = nullptr);
    ~Dialog();
    int userId;
    QVariantMap authResult;


public slots:

    QJsonDocument dataToJson(const int data);

private:
    Ui::Dialog *ui;
    tcpClient * client;

private:
    void registration();
    void connection();
    void serverAnswer(const QByteArray& data);

signals:
    void authSeccess(const QVariantMap&);

};

#endif // DIALOG_H
