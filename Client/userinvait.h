#ifndef USERINVAIT_H
#define USERINVAIT_H

#include <QDialog>
#include "mainwindow.h"

namespace Ui {
class UserInvait;
}

class UserInvait : public QDialog
{
    Q_OBJECT

public:
    explicit UserInvait(int userId, tcpClient *client, MainWindow *w, QWidget *parent = nullptr);
    ~UserInvait();
    void roomList();
    void invite();

private:
    Ui::UserInvait *ui;
    MainWindow *w;
    tcpClient *client;
    int idUser;
};

#endif // USERINVAIT_H
