#include "createroomdialog.h"
#include "ui_createroomdialog.h"


createroomdialog::createroomdialog(tcpClient *client, QWidget *parent) : QDialog(parent), ui(new Ui::createroomdialog)
{
    ui->setupUi(this);
    this->client = client;
    connect(ui->pbOk, &QPushButton::clicked, this, &createroomdialog::click);
    connect(client, &tcpClient::newDataAvaible, this, &createroomdialog::serverAnswer);
}

createroomdialog::~createroomdialog()
{
    delete ui;
}

void createroomdialog::click()
{
    QJsonObject jobject;
    jobject["commandCode"] = commandCode::roomCreated;
    jobject["roomName"] = ui->leRoomName->text();
    QJsonDocument doc(jobject);
    client->sendData(doc);
    accept();
}

void createroomdialog::serverAnswer(const QByteArray& data)
{
    QJsonDocument doc = QJsonDocument::fromJson(data);
    qDebug() << doc;

    int commandCode = doc.object()["commandCode"].toInt();
    switch (commandCode) {
        case commandCode::roomCreated:
            createRoomResult = doc.object().toVariantMap();
            emit createRoomSuccess(createRoomResult);
            accept();
            return;
    }
}
