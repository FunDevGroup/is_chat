#ifndef COMMANDLINE_H
#define COMMANDLINE_H
#include "QJsonDocument"
#include "QJsonObject"
#include "QSqlQuery"

class CommandLine
{
public:
    CommandLine();

private:
    void clearData(const QJsonDocument &com);
    void showUsers(const QJsonDocument &com);
    void clearMonitor();
};

#endif // COMMANDLINE_H
