#ifndef FIRSSTART_H
#define FIRSSTART_H

#include <QDialog>

namespace Ui {
class FirsStart;
}

class FirsStart : public QDialog
{
    Q_OBJECT

public:
    explicit FirsStart(QWidget *parent = nullptr);
    ~FirsStart();

private:
    Ui::FirsStart *ui;
};

#endif // FIRSSTART_H
